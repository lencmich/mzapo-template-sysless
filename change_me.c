#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_regs.h"

char *hello = "Hello World\n";

int main(int argc, char *argv[]) {
  uint32_t val_line=5;
  int i;

  for (int i = 0; i < 12; i++) {
    /* Check whether there is an empty space in TX FIFO. Loop until FIFO is full */
    while((*(volatile uint32_t*)(UART_REG_BASE + UART_STATUS_OFFSET) & UART_STATUS_TX_FULL));
    /* There is a free space, write character. */
    *(volatile uint32_t*)(UART_REG_BASE + UART_FIFO_OFFSET) = hello[i];
  }


  for (i=0; i<30; i++) {
     *(volatile uint32_t*)(SPILED_REG_BASE_PHYS + SPILED_REG_LED_LINE_o) = val_line;
     val_line<<=1;
     volatile int j = 0;
     while (j < 100000) {
      j++;
     }
  }

  return 0;
}