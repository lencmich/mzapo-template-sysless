Setup TFTP on your computer (see https://gitlab.fel.cvut.cz/otrees/rtems/work-and-ideas/-/wikis/RTEMS%20Build%20for%20mzAPO%20board#trivial-file-transfer-protocol-tftp-setup)

Initial setup

```console
$ mkdir /tftp/srv/zynq
$ cp system-2x-dc.bit autoscr-sysless.txt autoscr2img /tftp/srv/zynq
$ cd /tftp/srv/zynq
$ ./autoscr2img autoscr-sysless.txt
```

Application build:

```console
$ make
$ cp change_me /srv/tftp/
```

Reset board.